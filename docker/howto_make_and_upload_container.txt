# docker howto:

# install docker, eg. via
sudo snap install docker

# cd to tkwant/docker and build container locally
sudo docker build -t gitlab.kwant-project.org:5005/kwant/tkwant -f Dockerfile.debian .

# login at gitlab, use own kwant-gitlab credentials
sudo docker login gitlab.kwant-project.org:5005

# upload and finish
sudo docker push gitlab.kwant-project.org:5005/kwant/tkwant
sudo docker logout gitlab.kwant-project.org:5005

