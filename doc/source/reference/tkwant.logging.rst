:mod:`tkwant._logging` -- Logging
=================================

.. module:: tkwant._logging

Helper functions
----------------

.. autosummary::
    :toctree: generated

    make_logger
