:mod:`tkwant.onebody.solvers` -- Solvers for ordinary differential equations 
============================================================================
ODE Solvers for initial value problems. They are used interally to evolve
the one-body Schrödinger equation in time.

ODE Solvers
-----------
.. module:: tkwant.onebody.solvers
.. autosummary::
    :toctree: generated

    Scipy


